/**
 * Licensed Materials - Property of ReqPro.com / SmarterProcess © Copyright ReqPro 2020
 * Contact information: info@reqpro.com
 */

var artRef;
var moduleRef;
var internal;

var foundData = [];

$(function () {
  if (window.RM) {
    gadgets.window.adjustHeight();
    var prefs = new gadgets.Prefs();
    internal = prefs.getString("internal");
  } else {
    clearSelectedInfo();
    lockButton("#findButton");
    $(".status")
      .addClass("incorrect")
      .html("<b>Error:</b> The widget can be used only inside IBM Engineering DOORS Next.");
    gadgets.window.adjustHeight();
  }
});

function findAllArtifacts() {
  console.log(internal);
  // Blocking the button and starting creation process
  $("#log").attr("hidden", "hidden");
  $("#log").html(
    "This log message is displayed because there are more than 200 artifact in the module with broken links.<p><b>Log:</b></p></div>"
  );

  clearSelectedInfo();
  $("#status").addClass("warning");
  $("#status").html("<b>Message:</b> Searching for artifacts with broken links in primary text.");

  lockButton("#findButton");
  gadgets.window.adjustHeight();

  RM.Client.getCurrentConfigurationContext(async function (result) {
    if (result.code === RM.OperationResult.OPERATION_OK) {
      var context = result.data;
      var componentURL = context.localComponentUri;
      var configurationURL = context.localConfigurationUri;

      RM.Data.getContentsAttributes(
        moduleRef,
        [RM.Data.Attributes.NAME, RM.Data.Attributes.IDENTIFIER, RM.Data.Attributes.PRIMARY_TEXT],
        function (result) {
          if (result.code !== RM.OperationResult.OPERATION_OK) {
            clearSelectedInfo();
            $("#status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
          } else {
            var found = [];
            foundData = [];
            for (var i = 0; i < result.data.length; i++) {
              var item = result.data[i];
              var text = item.values[RM.Data.Attributes.PRIMARY_TEXT];

              if (text.indexOf(`href="`) > 0) {
                // test

                if (checkText(text, configurationURL)) {
                  found.push(item.ref);
                  foundData.push(item);
                }
              }
            }

            if (found.length == 0) {
              clearSelectedInfo();
              unlockButton("#findButton");
              $("#status")
                .addClass("correct")
                .html(`<b> Message:</b > None of artifacts in the module contains broken links.`);
              gadgets.window.adjustHeight();
            } else if (found.length > 200) {
              clearSelectedInfo();
              unlockButton("#findButton");
              $("#status")
                .addClass("warning")
                .html(
                  `<b> Message:</b > Found more than 200 artifacts. DOORS Next does not allow to select more than 200 artifacts in the module.`
                );
              $("#log").removeAttr("hidden");

              $("#log").html(
                `This log message is displayed because there are more than 200 artifact in the module with broken links.<p><b>Log:</b></p></div>`
              );

              foundData.forEach(function (item, index) {
                var id = item.values[RM.Data.Attributes.IDENTIFIER];
                var name = item.values[RM.Data.Attributes.NAME];

                $("<p></p>")
                  .html(`<p><ul><li>${id}: ${name}</li><ul></p>`)
                  .appendTo("#log")
                  .on("click", function () {
                    RM.Client.setSelection(foundData[index].ref);
                  });
              });
              gadgets.window.adjustHeight();
            } else {
              RM.Client.setSelection(found);
              clearSelectedInfo();
              unlockButton("#findButton");
              $("#status")
                .addClass("correct")
                .html(
                  `<b> Message:</b > Found ${found.length} artifacts with broken links and marked them as selected`
                );
              $("#log").removeAttr("hidden");

              $("#log").html("<b>Log:</b></div>");

              foundData.forEach(function (item, index) {
                var id = item.values[RM.Data.Attributes.IDENTIFIER];
                var name = item.values[RM.Data.Attributes.NAME];
                $("<p></p>")
                  .html(`<p><ul><li>${id}: ${name}</li><ul></p>`)
                  .appendTo("#log")
                  .on("click", function () {
                    RM.Client.setSelection(foundData[index].ref);
                  });
              });

              gadgets.window.adjustHeight();
            }
          }
          unlockButton("#findButton");
        }
      );
    } else {
      unlockButton("#findButton");
      $("#status").removeClass("warning correct incorrect");
      $("#status").addClass("incorrect").html("<b>Status:</b> Widget was not able to get project data.");
    }
  });
  // tutaj
}

// Subscribe to the Artifact Selection event
RM.Event.subscribe(RM.Event.ARTIFACT_SELECTED, function (selected) {
  gadgets.window.adjustHeight();
});

function clearSelectedInfo() {
  $("#status").removeClass("incorrect correct warning");
  gadgets.window.adjustHeight();
}

function checkIfModule(opResult) {
  if (opResult.code === RM.OperationResult.OPERATION_OK) {
    var attrs = opResult.data[0];

    if (attrs.values[RM.Data.Attributes.FORMAT] === RM.Data.Formats.MODULE) {
      moduleRef = attrs.ref;
      unlockButton("#findButton");
      $("#findButton").on("click", findAllArtifacts);
      $("#log").html(
        "This log message is displayed because there are more than 200 artifact in the module with broken links.<p><b>Log:</b></p></div>"
      );
    } else {
      lockButton("#findButton");
    }
  }
  gadgets.window.adjustHeight();
}

RM.Event.subscribe(RM.Event.ARTIFACT_OPENED, function (ref) {
  RM.Data.getAttributes(ref, RM.Data.Attributes.FORMAT, checkIfModule);
});

RM.Event.subscribe(RM.Event.ARTIFACT_CLOSED, function (ref) {
  $("#log").html(
    "This log message is displayed because there are more than 200 artifact in the module with broken links.<p><b>Log:</b></p></div>"
  );
  lockButton("#findButton");
  $("#log").attr("hidden", "hidden");
  gadgets.window.adjustHeight();
  moduleRef = null;
});

function checkText(text, configurationURL) {
  let regexp = /href="(.*?)"/g;
  var artifactsInternal = text.match(regexp);
  for (art of artifactsInternal) {
    tmp = art;
    tmp = tmp.replace(`href="`, "");
    tmp = tmp.slice(0, -1);
    if (internal == "true") {
      if (tmp.indexOf("/resources") > -1) {
        // ONLY INTERNAL RESOURCES
        if (checkLink(tmp, configurationURL)) return true;
      }
    } else {
      if (checkLink(tmp, configurationURL)) return true;
    }
  }
  return false;
}

function checkLink(link, configurationURI) {
  var projectArea_req = new XMLHttpRequest();
  projectArea_req.open("GET", link, false);
  projectArea_req.setRequestHeader("Accept", "application/xml");
  projectArea_req.setRequestHeader("Content-Type", "application/xml");
  projectArea_req.setRequestHeader("OSLC-Core-Version", "2.0");

  if (configurationURI != null || configurationURI != undefined || configurationURI != "") {
    projectArea_req.setRequestHeader("Configuration-Context", configurationURI);
  }

  try {
    projectArea_req.send();
    if (projectArea_req.status == "200") return false;
    else return true;
    return requirementURI;
  } catch (err) {
    return true;
  }
}

function lockButton(button) {
  $(button).attr("disabled", "disabled");
  $(button).removeClass("btn-primary");
  $(button).addClass("btn-secondary");
}

function unlockButton(button) {
  $(button).removeAttr("disabled");
  $(button).addClass("btn-primary");
  $(button).removeClass("btn-secondary");
}
